import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import ReactDOM from 'react-dom';
import { Route, Link, HashRouter as Router, Routes } from 'react-router-dom';
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import Linije from "./components/linije/Linije";
import EditLinije from "./components/linije/EditLinije";

class App extends React.Component {
  render() {
    return(
    <div>
      <Router>
        <Navbar expand bg="dark" variant="dark">
          <Navbar.Brand as={Link} to="/">
            AS
          </Navbar.Brand>
          <Nav>
            <Nav.Link as={Link} to="/linije">
              Linije
            </Nav.Link>
          </Nav>
        </Navbar>
        <Container style={{paddingTop:"25px"}}>
          <Routes>
            <Route path="/linije" element={<Linije/>} />
            <Route path="/linije/edit/:id" element={<EditLinije/>} />
            <Route path="/" element={<Home/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </Container>
      </Router>
    </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
