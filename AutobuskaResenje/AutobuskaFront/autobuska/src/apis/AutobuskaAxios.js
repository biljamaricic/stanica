import axios from 'axios';

var AutobuskaAxios = axios.create({
  baseURL: 'http://localhost:8080/api',
  /* other custom settings */
});

export default AutobuskaAxios;