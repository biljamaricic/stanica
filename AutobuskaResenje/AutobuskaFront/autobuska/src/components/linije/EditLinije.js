import React from "react";
import { Button, Form } from "react-bootstrap";
import { withNavigation, withParams } from "../../routeconf";
import AutobuskaAxios from "../../apis/AutobuskaAxios";

class EditLinije extends React.Component {

    constructor(props) {
        super(props);

        let linija = {
            brojMesta: 0,
            cenaKarte: 0.00,
            destinacija: "",
            vremePolaska: "",
            prevoznik: null,
            prevoznikId: -1
        }

        this.state = {
            prevoznici: [],
            linija: linija
        };
    }

    componentDidMount() {
        this.getPrevoznici();
        this.getLinija();
    }


    getLinija() {
        AutobuskaAxios.get('/linije/' + this.props.params.id)
            .then(res => {
                // handle success
                console.log(res);
                this.setState({ linija: res.data });
                var hh = this.state.linija
                console.log(hh)
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    getPrevoznici() {
        AutobuskaAxios.get('/prevoznici')
            .then(res => {
                // handle success
                console.log(res);
                this.setState({ prevoznici: res.data });
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    valueInputChanged(e) {
        let input = e.target;

        let name = input.name;
        let value = input.value;

        let linija = this.state.linija;
        linija[name] = value;

        this.setState({ linija: linija });
    }

    companySelectionChanged(e) {

        let prevoznikId = e.target.value;
        let prevoznik = this.state.prevoznici.find((prevoznik) => prevoznik.id == prevoznikId);

        let linija = this.state.linija;
        linija.company = prevoznik;

        this.setState({ linija: linija });
    }

    doEdit() {

        AutobuskaAxios.put('/linije/' + this.props.params.id, this.state.linija)
            .then(res => {
                // handle success
                console.log(res);
                alert('Uspesno izmenjeno!');
                this.props.navigate('/linije');
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }



    render() {
        return (
            <div>
                <h3>Izmena linije</h3>
                <Form style={{ marginTop: 10 }}>
                    <Form.Group>
                        <Form.Label htmlFor="brojMesta">Broj mesta</Form.Label>
                        <Form.Control id="lFreeSeats" name="brojMesta" value={this.state.linija.brojMesta} type="number" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="cenaKarte">Cena karte</Form.Label>
                        <Form.Control id="lTicketPrice" name="cenaKarte" value={this.state.linija.cenaKarte} type="number" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="destinacija">Destinacija</Form.Label>
                        <Form.Control id="lDestination" name="destinacija" value={this.state.linija.destinacija} type="text" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="vremePolaska">Vreme polaska</Form.Label>
                        <Form.Control id="lTime" name="vremePolaska" value={this.state.linija.vremePolaska} type="text" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Prevoznik</Form.Label>
                        <Form.Control
                            onChange={(event) => this.companySelectionChanged(event)}
                            value={this.state.linija.prevoznikId}
                            name="prevoznik"
                            as="select"
                        >
                            <option value={-1}></option>
                            {this.state.prevoznici.map((prevoznik) => {
                                return (
                                    <option value={prevoznik.id} key={prevoznik.id}>
                                        {prevoznik.naziv}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>

                    <Button style={{ marginTop: "25px", float: "right" }} onClick={(event) => { this.doEdit(event); }}>
                        Izmenite
                    </Button>
                </Form>
            </div>
        );
    }
}

export default withNavigation(withParams(EditLinije));