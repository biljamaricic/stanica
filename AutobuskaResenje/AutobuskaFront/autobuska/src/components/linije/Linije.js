import React from 'react';
import { Table, Button, Form, ButtonGroup } from 'react-bootstrap'
import AutobuskaAxios from './../../apis/AutobuskaAxios';
import { withNavigation } from './../../routeconf';

class Linije extends React.Component {
    constructor(props) {
        super(props);

        let linija = {
            freeSeats: 0,
            ticketPrice: 0.00,
            destination: "",
            time: "",
            company: null
        }

        this.state = {
            linija: linija,
            linije: [],
            prevoznici: [],
            pageNo: 0,
            totalPages: 1,
            search: { destinationS: "", companyIdS: -1, maxPriceS: 0 }
        };
    }

    componentDidMount() {
        this.getLinije(0);
        this.getPrevoznici();
        console.log(this.state.prevoznici)
    }

    getPrevoznici() {
        AutobuskaAxios.get('/prevoznici')
            .then(res => {
                // handle success
                console.log(res);
                this.setState({ prevoznici: res.data });
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    getLinije(page) {
        const conf = {
            params: {
                pageNo: page
            }
        }
        if (this.state.search.destinationS != "") {
            conf.params.destinacija = this.state.search.destinationS;
        }

        if (this.state.search.companyIdS != -1) {
            conf.params.prevoznikId = this.state.search.companyIdS;
        }

        if (this.state.search.maxPriceS != 0) {
            conf.params.maxCena = this.state.search.maxPriceS;
        }
        console.log(this.totalPages)
        AutobuskaAxios.get('/linije', conf)
            .then(res => {
                // handle success
                console.log(this.state.totalPages)
                console.log(res);
                this.setState({ linije: res.data, pageNo: page, totalPages: res.headers["total-pages"] });
                console.log(this.state.totalPages)
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    valueInputChanged(e) {
        let input = e.target;

        let name = input.name;
        let value = input.value;

        let linija = this.state.linija;
        linija[name] = value;

        this.setState({ linija: linija });
    }

    companySelectionChanged(e) {

        let prevoznikId = e.target.value;
        let prevoznik = this.state.prevoznici.find((prevoznik) => prevoznik.id == prevoznikId);

        let linija = this.state.linija;
        linija.company = prevoznik;

        this.setState({ linija: linija });
    }

    create() {
        let linija = this.state.linija;
        let linijaDTO = {
            brojMesta: linija.freeSeats,
            cenaKarte: linija.ticketPrice,
            destinacija: linija.destination,
            vremePolaska: linija.time,
            prevoznik: linija.company
        }

        AutobuskaAxios.post("/linije", linijaDTO)
            .then((res) => {
                // handle success
                console.log(res);

                alert("Line was added successfully!");
                window.location.reload();
            })
            .catch((error) => {
                // handle error
                console.log(error);
                alert("Error occured please try again!");
            });
    }

    searchValueInputChange(event) {
        let control = event.target;

        let name = control.name;
        let value = control.value;

        let search = this.state.search;
        search[name] = value;

        this.setState({ search: search });
    }

    doSearch() {
        this.getLinije(0);
    }

    doDelete(linijaId) {
        AutobuskaAxios.delete("/linije/" + linijaId)
            .then((res) => {
                var nextPage
                if (this.state.pageNo == this.state.totalPages - 1 && this.state.linije.length == 1) {
                    nextPage = this.state.pageNo - 1
                } else {
                    nextPage = this.state.pageNo
                }
                this.getLinije(nextPage);

            })
            .catch((error) => {
                alert("Nije uspelo brisanje.");
            });
    }

    goToEdit(linijaId) {
        this.props.navigate("/linije/edit/" + linijaId);
    }

    makeReservation(linijaId) {
        AutobuskaAxios.get("/linije/rezervacije/" + linijaId)
        .then((res) => {
            alert("Rezervisano!");
            window.location.reload();
        })
        .catch((error) => {
            alert("Nema slobodnih mesta za izabranu liniju!");
        });
    }

    renderLinije() {
        return this.state.linije.map((linija) => {
            return (
                <tr key={linija.id}>
                    <td>{linija.prevoznik.naziv}</td>
                    <td>{linija.destinacija}</td>
                    <td>{linija.brojMesta}</td>
                    <td>{linija.vremePolaska}</td>
                    <td>{linija.cenaKarte}</td>

                    <td><Button onClick={() => this.makeReservation(linija.id)}>Rezervisi</Button></td>
                    <td><Button variant="warning" onClick={() => this.goToEdit(linija.id)}>Izmeni</Button></td>
                    <td><Button variant="danger" onClick={() => this.doDelete(linija.id)}>Obrisi</Button></td>
                </tr>
            )
        })
    }


    render() {
        return (
            <div>
                <h1>Autobuska stanica</h1>
                <br />
                <h3>Dodavanje linija</h3>
                <Form style={{ marginTop: 10 }}>
                    <Form.Group>
                        <Form.Label htmlFor="freeSeats">Broj mesta</Form.Label>
                        <Form.Control id="lFreeSeats" name="freeSeats" type="number" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="ticketPrice">Cena karte</Form.Label>
                        <Form.Control id="lTicketPrice" name="ticketPrice" type="number" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="destination">Destinacija</Form.Label>
                        <Form.Control id="lDestination" name="destination" type="text" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="time">Vreme polaska</Form.Label>
                        <Form.Control id="lTime" name="time" type="text" onChange={(e) => this.valueInputChanged(e)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Prevoznik</Form.Label>
                        <Form.Control
                            onChange={event => this.companySelectionChanged(event)}
                            name="company"
                            as="select"
                        >
                            <option value={-1}></option>
                            {this.state.prevoznici.map((prevoznik) => {
                                return (
                                    <option value={prevoznik.id} key={prevoznik.id}>
                                        {prevoznik.naziv}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>

                    <Button style={{ marginTop: "25px", float: "right" }} onClick={(event) => { this.create(event); }}>
                        Dodaj
                    </Button>
                </Form>
                <br />
                <br />
                <br />

                { /*Pretraga*/}

                <h3>Pretraga linija</h3>
                <Form style={{ marginTop: 10 }}>
                    <Form.Group>
                        <Form.Label>Destinacija</Form.Label>
                        <Form.Control
                            value={this.state.search.destinationS}
                            name="destinationS"
                            as="input"
                            onChange={(e) => this.searchValueInputChange(e)}
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Prevoznik</Form.Label>
                        <Form.Control
                            onChange={(event) => this.searchValueInputChange(event)}
                            name="companyIdS"
                            value={this.state.search.companyIdS}
                            as="select"
                        >
                            <option value={-1}></option>
                            {this.state.prevoznici.map((prevoznik) => {
                                return (
                                    <option value={prevoznik.id} key={prevoznik.id}>
                                        {prevoznik.naziv}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Maksimalna cena</Form.Label>
                        <Form.Control
                            value={this.state.search.maxPriceS}
                            name="maxPriceS"
                            type="number"
                            as="input"
                            onChange={(e) => this.searchValueInputChange(e)}
                        ></Form.Control>
                    </Form.Group>

                    <Button style={{ marginTop: 25, float: "right" }} onClick={() => this.doSearch()}>Trazi</Button>
                </Form>
                <br />
                <br />

                {/*Lista*/}

                <br />
                <br />
                <ButtonGroup style={{ marginTop: 20, marginBottom: 15, float: "right" }}>
                    <Button
                        style={{ margin: 3, width: 90 }}
                        disabled={this.state.pageNo == 0} onClick={() => this.getLinije(this.state.pageNo - 1)}>
                        Prethodna
                    </Button>
                    <Button
                        style={{ margin: 3, width: 90 }}
                        disabled={this.state.pageNo == this.state.totalPages - 1} onClick={() => this.getLinije(this.state.pageNo + 1)}>
                        Sledeca
                    </Button>
                </ButtonGroup>
                <Table bordered striped style={{ marginTop: 5, marginBottom: 300 }}>
                    <thead>
                        <tr>
                            <th>Naziv prevoznika</th>
                            <th>Destinacija</th>
                            <th>Broj mesta</th>
                            <th>Vreme polaska</th>
                            <th>Cena karte</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderLinije()}
                    </tbody>
                </Table>
            </div >
        );
    }
}

export default withNavigation(Linije);