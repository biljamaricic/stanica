package com.ftninformatika.jwd.modul3.AutobuskaStanica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Prevoznik;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.repository.PrevoznikRepository;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.PrevoznikService;

@Service
public class JpaPrevoznikService implements PrevoznikService {
	
	@Autowired
	private PrevoznikRepository prevoznikRepository;

	@Override
	public List<Prevoznik> findAll() {
		return prevoznikRepository.findAll();
	}

	@Override
	public Prevoznik save(Prevoznik prevoznik) {		
		return prevoznikRepository.save(prevoznik);
	}

	@Override
	public Prevoznik findOne(Long id) {
		return prevoznikRepository.findOneById(id);
	}
	
	

}
