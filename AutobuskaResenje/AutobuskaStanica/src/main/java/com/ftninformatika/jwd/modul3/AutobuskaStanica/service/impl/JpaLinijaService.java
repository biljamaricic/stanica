package com.ftninformatika.jwd.modul3.AutobuskaStanica.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.repository.LinijaRepository;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.LinijaService;

@Service
public class JpaLinijaService implements LinijaService {

	@Autowired
	private LinijaRepository linijaRepository;
	
	@Override
	public Page<Linija> findAll(int page) {
		return linijaRepository.findAll(PageRequest.of(page, 4));
	}

	@Override
	public Linija findOne(Long id) {
		return linijaRepository.findOneById(id);
	}

	@Override
	public Linija save(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija update(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija delete(Long id) {
		Linija linija = findOne(id);
		if(linija != null) {
			linija.getPrevoznik().getLinije().remove(linija);
			linijaRepository.delete(linija);
			return linija;
		}
		return null;
		
	}

	@Override
	public Page<Linija> find(String destinacija, Long prevoznikId, Double maxCena, Integer pageNo) {
		if (maxCena == null) {
            maxCena = Double.MAX_VALUE;
        }
		if (destinacija == null) {
            destinacija = "";
        }
		if (prevoznikId == null) {
			return linijaRepository.findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThan(destinacija, maxCena, PageRequest.of(pageNo,4));
        }
		return linijaRepository.findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanAndPrevoznikId(destinacija, maxCena, prevoznikId, PageRequest.of(pageNo,4));
	}

	
	
}
