package com.ftninformatika.jwd.modul3.AutobuskaStanica.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Prevoznik;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.support.PrevoznikDtoToPrevoznik;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.support.PrevoznikToPrevoznikDto;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.web.dto.PrevoznikDTO;

@RestController
@RequestMapping(value = "/api/prevoznici", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PrevoznikController {
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private PrevoznikDtoToPrevoznik toPrevoznik;
	
	@Autowired
	private PrevoznikToPrevoznikDto toPrevoznikDto;
	
//	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping
	public ResponseEntity<List<PrevoznikDTO>> getAll() {
		
		List<Prevoznik> prevoznici = prevoznikService.findAll();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		if(prevoznici.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>(toPrevoznikDto.convert(prevoznici), headers, HttpStatus.OK);
		}
	}
	
//	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDTO> create(@Valid @RequestBody PrevoznikDTO prevoznikDTO){
		Prevoznik prevoznik = toPrevoznik.convert(prevoznikDTO);
		System.out.println(prevoznik);
		Prevoznik sacuvaniPrevoz = prevoznikService.save(prevoznik);
		
		return new ResponseEntity<>(toPrevoznikDto.convert(sacuvaniPrevoz), HttpStatus.CREATED);
		
	}
	
}
