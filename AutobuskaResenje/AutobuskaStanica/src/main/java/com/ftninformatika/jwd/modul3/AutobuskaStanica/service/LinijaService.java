package com.ftninformatika.jwd.modul3.AutobuskaStanica.service;

import org.springframework.data.domain.Page;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;

public interface LinijaService {
	
	Page<Linija> findAll(int page);
	
	Page<Linija> find(String destinacija, Long prevoznikId, Double maxCena, Integer pageNo);
	
	Linija findOne(Long id);
	
	Linija save(Linija linija);
	
	Linija update(Linija linija);
	
	Linija delete(Long id);
	
}
