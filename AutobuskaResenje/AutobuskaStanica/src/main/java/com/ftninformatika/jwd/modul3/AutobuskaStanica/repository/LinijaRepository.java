package com.ftninformatika.jwd.modul3.AutobuskaStanica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;

@Repository
public interface LinijaRepository extends JpaRepository<Linija, Long>{
	Linija findOneById(Long id);
	
	Page<Linija> findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThan(String destinacija, Double maxCena, Pageable pageable);
	Page<Linija> findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanAndPrevoznikId(String destinacija, Double maxCena, Long prevoznikId, Pageable pageable);
	
}
