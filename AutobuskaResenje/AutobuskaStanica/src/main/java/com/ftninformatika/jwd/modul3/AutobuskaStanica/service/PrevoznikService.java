package com.ftninformatika.jwd.modul3.AutobuskaStanica.service;

import java.util.List;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Prevoznik;

public interface PrevoznikService {
	
	Prevoznik findOne(Long id);
	
	List<Prevoznik> findAll();
	
	Prevoznik save(Prevoznik prevoznik);
	
}
