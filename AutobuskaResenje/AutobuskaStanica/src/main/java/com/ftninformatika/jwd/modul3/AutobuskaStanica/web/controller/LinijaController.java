package com.ftninformatika.jwd.modul3.AutobuskaStanica.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Rezervacija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.LinijaService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.RezervacijaService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.support.LinijaDtoToLinija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.support.LinijaToLinijaDto;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.web.dto.LinijaDTO;

@RestController
@RequestMapping(value = "/api/linije", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class LinijaController {
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private LinijaDtoToLinija toLinija;
	
	@Autowired
	private LinijaToLinijaDto toLinijaDto;
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping
	public ResponseEntity<List<LinijaDTO>> getAll(@RequestParam(required = false) String destinacija, 
			@RequestParam(required = false) Long prevoznikId,
			@RequestParam(required = false) Double maxCena,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo){
		
		Page<Linija> page;
		System.out.println(destinacija);
		if (destinacija != null || prevoznikId != null || maxCena != null) {
			page = linijaService.find(destinacija, prevoznikId, maxCena, pageNo);
		} else {
			page = linijaService.findAll(pageNo);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
//		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Expose-Headers", "*");
		
		System.out.println(page.getTotalPages());
		
		return new ResponseEntity<>(toLinijaDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/{id}")
	public ResponseEntity<LinijaDTO> getOne(@PathVariable Long id){
		Linija linija = linijaService.findOne(id);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		
		if(linija != null) {
			return new ResponseEntity<>(toLinijaDto.convert(linija), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> create(@Valid @RequestBody LinijaDTO linijaDTO){
		Linija linija = toLinija.convert(linijaDTO);
		Linija sacuvanaLinija = linijaService.save(linija);
		
		//HttpHeaders headers = new HttpHeaders();
		//headers.add("Access-Control-Allow-Origin", "*");
		return new ResponseEntity<>(toLinijaDto.convert(sacuvanaLinija),  HttpStatus.CREATED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> update(@PathVariable Long id, @Valid @RequestBody LinijaDTO linijaDTO){
		
		if(!id.equals(linijaDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Linija linija = toLinija.convert(linijaDTO);
		Linija sacuvanaLinija = linijaService.update(linija);
		
		return new ResponseEntity<>(toLinijaDto.convert(sacuvanaLinija), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		Linija obrisanaLinija = linijaService.delete(id);
		if(obrisanaLinija != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/rezervacije/{id}")
	public ResponseEntity<Void> reservation(@PathVariable Long id){
		Linija linija = linijaService.findOne(id);
		
		int brojMesta = linija.getBrojMesta();
		
		if(brojMesta > 0) {
			brojMesta--;
			linija.setBrojMesta(brojMesta);
			linijaService.update(linija);
			
			Rezervacija rezervacija = new Rezervacija(linija);
			rezervacijaService.save(rezervacija);
			
			return new ResponseEntity<>(HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
