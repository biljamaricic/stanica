package com.ftninformatika.jwd.modul3.AutobuskaStanica.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.LinijaService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.web.dto.LinijaDTO;

@Component
public class LinijaDtoToLinija implements Converter<LinijaDTO, Linija>{
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Override
	public Linija convert(LinijaDTO dto) {
		
		Linija linija;
		
		if(dto.getId() == null) {
			linija = new Linija();
		}else {
			linija = linijaService.findOne(dto.getId());
		}
		
		if(linija != null) {
			linija.setBrojMesta(dto.getBrojMesta());
			linija.setCenaKarte(dto.getCenaKarte());
			linija.setVremePolaska(dto.getVremePolaska());
			linija.setDestinacija(dto.getDestinacija());
			linija.setPrevoznik(prevoznikService.findOne(dto.getPrevoznik().getId()));
		}
		
		return linija;
	}

}
