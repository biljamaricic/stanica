package com.ftninformatika.jwd.modul3.AutobuskaStanica.service;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Rezervacija;

public interface RezervacijaService {
	Rezervacija save(Rezervacija rezervacija);
}
