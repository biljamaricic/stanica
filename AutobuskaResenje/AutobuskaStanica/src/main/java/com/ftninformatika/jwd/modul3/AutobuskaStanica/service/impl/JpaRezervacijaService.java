package com.ftninformatika.jwd.modul3.AutobuskaStanica.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Rezervacija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.repository.RezervacijaRepository;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.RezervacijaService;

@Service
public class JpaRezervacijaService implements RezervacijaService {
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;
	
	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		return rezervacijaRepository.save(rezervacija);
	}

}
