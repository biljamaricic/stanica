package com.ftninformatika.jwd.modul3.AutobuskaStanica.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Prevoznik;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.web.dto.PrevoznikDTO;

@Component
public class PrevoznikDtoToPrevoznik implements Converter<PrevoznikDTO, Prevoznik>{
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Override
	public Prevoznik convert(PrevoznikDTO dto) {
		
		Prevoznik prevoznik;
		
		if(dto.getId() == null) {
			prevoznik = new Prevoznik();
		}else {
			prevoznik = prevoznikService.findOne(dto.getId());
		}
		
		if(prevoznik != null) {
			prevoznik.setNaziv(dto.getNaziv());
			prevoznik.setAdresa(dto.getAdresa());
			prevoznik.setPIB(dto.getPIB());
			
		}
		
		return prevoznik;
	}

	
}
