package com.ftninformatika.jwd.modul3.AutobuskaStanica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.AutobuskaStanica.model.Linija;
import com.ftninformatika.jwd.modul3.AutobuskaStanica.web.dto.LinijaDTO;

@Component
public class LinijaToLinijaDto implements Converter<Linija, LinijaDTO>{
	
	@Autowired
	PrevoznikToPrevoznikDto prevoznikToPrevoznikDto;
	
	@Override
	public LinijaDTO convert(Linija linija) {
		LinijaDTO dto = new LinijaDTO();
		dto.setId(linija.getId());
		dto.setBrojMesta(linija.getBrojMesta());
		dto.setCenaKarte(linija.getCenaKarte());
		dto.setVremePolaska(linija.getVremePolaska());
		dto.setDestinacija(linija.getDestinacija());
		dto.setPrevoznik(prevoznikToPrevoznikDto.convert(linija.getPrevoznik()));
		dto.setPrevoznikId(linija.getPrevoznik().getId());
		return dto;
	}
	
	public List<LinijaDTO> convert(List<Linija> linije){
		List<LinijaDTO> linijeDto = new ArrayList<>();
		
		for(Linija linija : linije) {
			linijeDto.add(convert(linija));
		}
		
		return linijeDto;
	}
}
