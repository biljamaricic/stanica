INSERT INTO prevoznici (id, pib, adresa, naziv) VALUES (1, '14514', 'Gogoljeva 30', 'Lasta');
INSERT INTO prevoznici (id, pib, adresa, naziv) VALUES (2, '13544', 'Gagarinova 56', 'Severtrans');
INSERT INTO prevoznici (id, pib, adresa, naziv) VALUES (3, '34264', 'Puskinova 30', 'Nis Express');
INSERT INTO prevoznici (id, pib, adresa, naziv) VALUES (4, '94514', 'Cirpanova 39', 'Subotica trans');

INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (1, 32, 590.0, 'Beograd', '21:00', 1);
INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (2, 54, 1590.0, 'Novi Sad', '20:00', 2);
INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (3, 22, 840.0, 'Subotica', '18:00', 1);
INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (4, 87, 950.0, 'Beograd', '17:00', 3);
INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (5, 24, 470.0, 'Nis', '11:00', 4);
INSERT INTO linije (id, broj_mesta, cena_karte, destinacija, vreme_polaska, prevoznik_id) VALUES (6, 30, 2590.0, 'Budimpesta', '15:00', 4);